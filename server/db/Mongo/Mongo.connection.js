import mongoose from 'mongoose';
import autoIncrement from "mongoose-auto-increment";

mongoose.connect("mongodb://localhost/likeme");
mongoose.Promise = global.Promise;

export const connection = mongoose.connection;

let usersTable = null;
let articlesTable = null;

/*connection.once('open', function () {
    console.log('connection established');

    connection.db.collection("users", function(err, collection){
        usersTable = collection;
    });

    connection.db.collection("articles", function(err, collection){
        articlesTable = collection;
    });
});*/