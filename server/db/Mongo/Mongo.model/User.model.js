import passportLocalMongoose from 'passport-local-mongoose';
import mongoose from 'mongoose';
import autoIncrement from "mongoose-auto-increment";
import {connection} from "../Mongo.connection";
//import mongoose from '../Mongo.connection';

autoIncrement.initialize(connection);

const userSchema = mongoose.Schema({
    login: String,
    password: String,
    first_name: String,
    last_name: String,
    email: String,
    isAdmin: { type: Number, default: 0 },
    likedArticles: { type: [Number], default: [] }
});

userSchema.plugin(passportLocalMongoose);
userSchema.plugin(autoIncrement.plugin, { model: 'users', field: 'user_id' });

export const User = mongoose.model('users', userSchema);