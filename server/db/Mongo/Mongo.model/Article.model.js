import passportLocalMongoose from 'passport-local-mongoose';
import mongoose from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';
import {connection} from "../Mongo.connection";

//import mongoose from '../Mongo.connection';
autoIncrement.initialize(connection);

const articleSchema = mongoose.Schema({
    author_name: { type : String , unique : false },
    name: { type : String , unique : false },
    text: { type : String , unique : false },
    like_number: { type : Number , unique : false, default: 0 },
    createdAt: { type: Date, default: Date.now },
});

articleSchema.plugin(passportLocalMongoose);
articleSchema.plugin(autoIncrement.plugin, { model: 'articles', field: 'article_id'});

export const Article = mongoose.model('articles', articleSchema);

