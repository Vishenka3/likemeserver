import passport from "passport/lib/index";
import jwt from "jsonwebtoken";

import { User } from './Mongo.model/User.model';
import { Article } from "./Mongo.model/Article.model";

export default class Session {

    static addUser(userTemp, response, request){
        console.log('adding n session: ',  userTemp);
        let userToSave = new User({
            login: userTemp.username,
            password: userTemp.password,
            first_name: userTemp.firstname,
            last_name: userTemp.lastname,
            email: userTemp.username,
        });

        userToSave.save( (err, user) => {
            if (err) return response.json(err);
            console.log('added successfully');

            passport.authenticate('local', { session: true }, (err, userToSave, info) => {
                console.log('err', err);
                console.log('user', user);

                if (err || !userToSave) {
                    return response.status(400).json({
                        message: info ? info.message : 'Login failed',
                        userToSave,
                    });
                }
                request.login(userToSave, { session: false }, (err) => {
                    if (err) {
                        response.send(err);
                    }

                    const token = jwt.sign(user.toJSON(), 'secretkey');
                    console.log('token', token);
                    console.log('user sending', user);
                    return response.json({ user, token });
                });
            })(request, response);
        });
    }

    static findUser(login, password, cb){
        User.findOne({ login: login }, (err, foundedUser) => {
            if (err) return cb(err);
            console.log(`ras: `, foundedUser);
            if (!foundedUser) {
                console.log(`лулы: `);
                return cb(null, false, { message: 'Incorrect email or password.' });
            }
            return cb(null, foundedUser, {
                message: 'Logged In Successfully',
            });
        });
    }

    static findOneUserById(jwtPayload, cb){
        User.find({ user_id: jwtPayload.id }, (err, foundedUser) => {
            if (err) return cb(err);
            console.log('founded: ', foundedUser);
            cb(null, foundedUser)
        });
    }

    static findUserById(id, done){
        User.find({ user_id: jwtPayload.id }, (err, foundedUser) => {
            if (err) return done(err);
            console.log('founded: ', foundedUser);
            done(null, foundedUser)
        });
    }

    static findAllArticles(req, res){
        Article.find( (err, foundedArticles) => {
            if (err) {
                console.log('err: ', err);
                return  res.json(err);
            }
            console.log('founded: ', foundedArticles);
            res.json(foundedArticles);
        });
    }

    static addNewArticle(decoded, request, response){
        let articleToSave = new Article({
            author_id: decoded.payload.user_id,
            author_name: `${decoded.payload.first_name} ${decoded.payload.last_name}`,
            name: request.topic,
            text: request.text,
        });

        articleToSave.save( (err, articleToSave) => {
            if (err) {
                console.log('article adding err: ', err);
                return response.json(err);
            }
            console.log('article added successfully', articleToSave);
            response.json('Article added successfully');
        });
    }

    static manageLike(decoded, request, response) {

        User.findOne({user_id: decoded.payload.user_id, likedArticles: request.article_id }, (err, foundedUser) => {
            if (foundedUser) { //Если лайк не ставил
                User.findOneAndUpdate({user_id: decoded.payload.user_id}, {$pull: {likedArticles: request.article_id}},
                    (err, noErr) => {if (err) console.log(err);});

                Article.findOneAndUpdate({article_id: request.article_id}, {$inc: {like_number: -1}},
                    (err, noErr) => {if (err) console.log(err);});

                response.json(true);
            } else {    //Если ставил
                User.findOneAndUpdate({user_id: decoded.payload.user_id}, {$push: {likedArticles: request.article_id}},
                    (err, noErr) => {if (err) console.log(err);});

                Article.findOneAndUpdate({article_id: request.article_id}, {$inc: {like_number: 1}},
                    (err, noErr) => {if (err) console.log(err);});

                response.json(false);
            }
        });

    }
}