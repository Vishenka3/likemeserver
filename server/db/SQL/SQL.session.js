import passport from "passport/lib/index";
import jwt from "jsonwebtoken";

import {User} from "./SQL.model/User.model";
import {Article} from "./SQL.model/Article.model";
import {Like} from "./SQL.model/Like.model";

export default class Session{

    static addUser(userTemp, response) {
        User.create({
            login: userTemp.username,
            password: userTemp.password,
            first_name: userTemp.firstname,
            last_name: userTemp.lastname,
            email: userTemp.email,
        })
            .then((res) => {
                console.log(res);

                passport.authenticate('local', { session: true }, (err, user, info) => {

                    console.log('err', err);
                    console.log('user', user);
                    if (err || !user) {
                        return response.status(400).json({
                            message: info ? info.message : 'Login failed',
                            user,
                        });
                    }

                    request.login(user, { session: false }, (err) => {
                        if (err) {
                            response.send(err);
                        }

                        const token = jwt.sign(user.toJSON(), 'secretkey');

                        return response.json({ user, token });
                    });
                })(request, response);
            })
            .catch((error) => {
                console.log(error);
                response.json(error.errors[0]);
            });
    }

    static findUser(login, password, cb){
        User.findOne({ where: { login } })
            .then((user) => {
                console.log(`ras: `, user);
                if (!user) {
                    return cb(null, false, { message: 'Incorrect email or password.' });
                }
                return cb(null, user, {
                    message: 'Logged In Successfully',
                });
            })
            .catch(err => cb(err))
    }

    static findOneUserById(jwtPayload, cb){
        User.findOneById(jwtPayload.id)
            .then(user => cb(null, user))
            .catch(err => cb(err));
    }

    static findUserById(id, done){
        User.findById(id, (err, user) => {
            err
                ? done(err)
                : done(null, user);
        });
    }

    static findAllArticles(req, res){
        Article.findAll().then((result) => {
            res.json(result);
        });
    }

    static addNewArticle(decoded, request, response){
        Article.create({
            author_id: decoded.payload.user_id,
            author_name: `${decoded.payload.first_name} ${decoded.payload.last_name}`,
            name: request.topic,
            text: request.text,
        })
            .then((res) => {
                console.log(res);
                response.json('Article added successfully');
            })
            .catch((error) => {
                console.log(error);
                response.json(error.errors[0]);
            });
    }

    static managelike(decoded, request, response){
        Like.findOrCreate(
            {where: {article_id:request.article_id, user_id: decoded.payload.user_id}, defaults: {
                    user_id: decoded.payload.user_id,
                    article_id: request.article_id,
                }})
            .then((res) => {
                let pepka = 0;
                Article.findById(request.article_id).then(res => {
                    pepka = res.dataValues.like_number;
                })
                    .then( () => {
                        if(res[0]._options.isNewRecord) {
                            Article.update({
                                like_number: ++pepka,
                            }, {
                                where: {article_id: request.article_id}
                            })
                                .then(res => {
                                    console.log('update', res);
                                    response.json(true);
                                });
                        } else {
                            Like.destroy({where: {article_id:request.article_id}});

                            Article.update({
                                like_number: --pepka,
                            }, {
                                where: {article_id: request.article_id}
                            })
                                .then(res => {
                                    console.log('update', res);
                                    response.json(false);
                                });
                        }
                    });
            })
            .catch((error) => {
                console.log(error);
                response.json(error);
            });
    }
}