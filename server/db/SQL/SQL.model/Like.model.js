import { sequelize } from '../SQL.connection';
import { User } from './User.model';
import { Article } from './Article.model';

const Sequelize = require('sequelize');

export const Like = sequelize.define('likes', {
    like_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    },
    /*user_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0,
    },*/
}, {
    updatedAt: false,
    deletedAt: false,
});

Like.belongsTo(User, {foreignKey: 'user_id', targetKey: 'user_id'});
Like.belongsTo(Article, {foreignKey: 'article_id', targetKey: 'article_id'});

Like.sync({ force: false })
    .then(() => console.log('created likes'))
    .catch((err) => {
        console.log(err);
    });