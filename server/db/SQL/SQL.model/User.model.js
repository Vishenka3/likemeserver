import { sequelize } from '../SQL.connection';

import Sequelize from 'sequelize';

export const User = sequelize.define('users', {
    user_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    },
    login: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    first_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    last_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    isAdmin: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0,
    },

}, {
    createdAt: false,
    updatedAt: false,
    deletedAt: false,
});

User.sync({ force: false })
    .then(() => console.log('created users'))
    .catch((err) => {
        console.log(err);
    });
