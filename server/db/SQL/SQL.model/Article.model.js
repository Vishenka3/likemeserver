import { sequelize } from '../SQL.connection';
import { User } from './User.model';

const Sequelize = require('sequelize');

export const Article = sequelize.define('articles', {
    article_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        autoIncrement: true,
        unique: true,
        primaryKey: true,
    },
    author_name:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    text: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    like_number: {
        type: Sequelize.INTEGER,
        allowNull: true,
        defaultValue: 0,
    },
}, {
    updatedAt: false,
    deletedAt: false,
});

Article.belongsTo(User, {foreignKey: 'author_id', targetKey: 'user_id'});

Article.sync({ force: false })
    .then(() => console.log('created articles'))
    .catch((err) => {
        console.log(err);
    });