import passport from 'passport';
import passportJWT from 'passport-jwt';
//Mongo SQL
import Session from '../db/Mongo/Mongo.session';

const ExtractJWT = passportJWT.ExtractJwt;

const LocalStrategy = require('passport-local').Strategy;

const JWTStrategy = passportJWT.Strategy;


passport.use('local', new LocalStrategy({
    usernameField: 'regUsername',
    passwordField: 'regPassword',
},
    ((login, password, cb) => Session.findUser(login, password, cb))));

passport.use(new JWTStrategy({
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey: 'secretkey',
},
    ((jwtPayload, cb) => {
        return Session.findOneUserById(jwtPayload, cb);
    }
    )));

passport.serializeUser((user, done) => {
    done(null, user.id);
});


passport.deserializeUser((id, done) => {
    Session.findUserById(id, done);
});
