import express from 'express';
import bodyParser from 'body-parser';
import jwt from 'jsonwebtoken';

import passport from 'passport/lib/index';
//Mongo SQL
import Session from '../db/Mongo/Mongo.session';

const jsonParser = bodyParser.json();

const router = express.Router();
/* GET home page. */
router.get('/', (req, res) => {
    res.render('index');
});

router.get('/articles', (req, res) => {
    Session.findAllArticles(req, res);
});

router.post('/signin', jsonParser, (request, response) => {

    if (!request.body) return response.sendStatus(400);

    const userTemp = {};
    userTemp.username = request.body.regUsername;
    userTemp.password = request.body.regPassword;
    userTemp.password2 = request.body.regRetypePassword;
    userTemp.firstname = request.body.regFirstname;
    userTemp.lastname = request.body.regLastname;
    userTemp.email = request.body.regEmail;

    console.log('trying to add');
    Session.addUser(userTemp, response, request);
});

router.post('/login', jsonParser, (req, res, next) => {

    passport.authenticate('local', { session: true }, (err, user, info) => {
        console.log('user: ', user);

        if (err || !user) {
            return res.status(420).json({
                message: info ? info.message : 'Login failed',
                user,
            });
        }

        req.login(user, { session: false }, (err) => {
            if (err) {
                res.send(err);
            }
            console.log(user);
            const token = jwt.sign(user.toJSON(), 'secretkey');

            return res.json({ user, token });
        });
    })(req, res);

});

router.get('/signout', function(req, res) {
    req.logout();
});

router.post('/hello', jsonParser, (request, response) => {

    if (!request.body) return response.sendStatus(400);
    const tokenTemp = request.headers.authorization;
    const decoded = jwt.decode(tokenTemp, {complete: true});

    return response.json(decoded.payload);
});

router.post('/new-article', jsonParser, (request, response) => {

    if (!request.body) return response.sendStatus(400);

    const tokenTemp = request.headers.authorization;
    const decoded = jwt.decode(tokenTemp, {complete: true});
    Session.addNewArticle(decoded, request.body, response);
});

router.post('/like', jsonParser, (request, response) => {

    if (!request.body) return response.sendStatus(400);

    const tokenTemp = request.headers.authorization;
    const decoded = jwt.decode(tokenTemp, {complete: true});
    // console.log(decoded);
    // console.log(request.body.article_id);

    Session.manageLike(decoded, request.body, response);

});

export default router;
